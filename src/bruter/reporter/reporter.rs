use ascii::AsciiString;
use std::marker;


pub trait Reporter: marker::Send {
    fn report_non_preauth_user(&mut self, username: &AsciiString, asrep: &kerbeiros::AsRep);
    fn report_invalid_username(&mut self, username: &AsciiString);
    fn report_disabled_user(&mut self, username: &AsciiString);
    fn report_valid_username(&mut self, username: &AsciiString);
    fn report_valid_username_password(&mut self, username: &AsciiString, password: &String, credential: &kerbeiros::Credential);
    fn report_error(&mut self, username: &AsciiString, password: &String, error: &kerbeiros::Error);
    fn report_krb_error(&mut self, username: &AsciiString, password: &String, krb_error: &kerbeiros::KrbError);
    fn report_attack_stopped_by_errors(&mut self);
    fn report_attack_summary(&mut self, discovered_users_count: Option<usize>, non_preauth_count: usize);
}