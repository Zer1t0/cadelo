use crate::args::formats::*;
use std::fs::File;
use std::io::Write;
use ascii::AsciiString;
use super::Reporter;

const VERBOSITY_HIGH: u8 = 1;
const VERBOSITY_SUPER_HIGH: u8 = 2;

pub struct PrintReporter {
    ticket_format: Option<TicketFormat>,
    non_preauth_format: CrackFormat,
    non_preauth_file: Option<File>,
    verbosity: u8
}


impl PrintReporter {

    pub fn new(
        ticket_format: Option<TicketFormat>,
        non_preauth_format: CrackFormat,
        non_preauth_file: Option<File>,
        verbosity: u8
    ) -> Self {
        return Self{
            ticket_format,
            non_preauth_format,
            non_preauth_file,
            verbosity
        }
    }

    fn arr_u8_to_hexa_string(array: &[u8]) -> String {
        let mut hexa_string = String::new();
        for item in array.iter() {
            hexa_string.push_str(&format!("{:x}", item));
        }
        return hexa_string;
    }

    fn save_credential_in_file(&self, username: &AsciiString, credential: &kerbeiros::Credential) {
        if let Some(ticket_format) = &self.ticket_format {
            match ticket_format {
                TicketFormat::Ccache => {
                    self.save_credential_in_ccache_file(username, credential);
                }
                TicketFormat::Krb => {
                    self.save_credential_in_krb_file(username, credential);
                }
            }
        }
    }

    fn save_credential_in_ccache_file(&self, username: &AsciiString, credential: &kerbeiros::Credential) {
        let filename = format!("{}.ccache", username);

        match credential.save_into_ccache_file(&filename) {
            Ok(_) => {
                println!("[+] Saved {} ticket into {}", username, filename);
            }
            Err(error) => {
                println!("[!] Error saving {} ticket: {}", username, error);
            }
        }
    }

    fn save_credential_in_krb_file(&self, username: &AsciiString, credential: &kerbeiros::Credential) {
        let filename = format!("{}.krb", username);

        match credential.save_into_krb_cred_file(&filename) {
            Ok(_) => {
                println!("[+] Saved {} ticket into {}", username, filename);
            }
            Err(error) => {
                println!("[!] Error saving {} ticket: {}", username, error);
            }
        }
    }

    fn should_print_invalid_username(&self) -> bool {
        return self.verbosity >= VERBOSITY_SUPER_HIGH;
    }

    fn should_print_valid_username(&self) -> bool {
        return self.verbosity >= VERBOSITY_HIGH;
    }

}

impl Reporter for PrintReporter {

    fn report_invalid_username(&mut self, username: &AsciiString) {
        if self.should_print_invalid_username() {
            println!("[-] Invalid {}", username);
        }
    }

    fn report_disabled_user(&mut self, username: &AsciiString) {
        println!("[!] Blocked/Disabled {}", username);
    }

    fn report_valid_username(&mut self, username: &AsciiString) {
        if self.should_print_valid_username() {
            println!("[*] Valid {}", username);
        }
    }

    fn report_non_preauth_user(&mut self, username: &AsciiString, asrep: &kerbeiros::AsRep) {
        println!("[*] Valid {} : No Preauth", username);

        let etype = asrep.enc_part_etype();
        let realm = asrep.crealm();
        let salt = asrep.encryption_salt();
        let ciphertext = asrep.enc_part_cipher();
        let salt_hexa = Self::arr_u8_to_hexa_string(&salt);
        let cipher_hexa = Self::arr_u8_to_hexa_string(&ciphertext);

        let asrep_crack_string = match self.non_preauth_format {
            CrackFormat::Hashcat => {
                format!("$krb5asrep${}${}@{}:{}${}", etype, username, realm, salt_hexa, cipher_hexa)
            }
            CrackFormat::John => {
                format!("$krb5asrep${}@{}:{}${}", username, realm, salt_hexa, cipher_hexa)
            }
        };

        if let Some(non_preauth_file) = &mut self.non_preauth_file {
            non_preauth_file.write_fmt(format_args!("{}\n",asrep_crack_string)).unwrap_or_else(|error|{
                println!("[!] Error writing non pre-auth user as-rep: {}", error);
            });
        }

        println!("[*] To crack {} {}", username, asrep_crack_string);
    }


    fn report_valid_username_password(&mut self, 
        username: &AsciiString, password: &String, credential: &kerbeiros::Credential
    ) {
        println!("[+] STUPENDOUS {}:{}", username, password);
        self.save_credential_in_file(username, credential);
    }

    fn report_krb_error(&mut self, username: &AsciiString, password: &String, krb_error: &kerbeiros::KrbError) {
        println!(
            "[x] Error checking {}:{} => KRB-ERROR response ({}) {}", 
            username, password, krb_error.error_code(), 
            kerbeiros::error_code_to_string(krb_error.error_code())
        );
    }

    fn report_error(&mut self, username: &AsciiString, password: &String, error: &kerbeiros::Error) {
        println!("[x] Error checking {}:{} => {}", username, password, error);
    }

    fn report_attack_stopped_by_errors(&mut self) {
        println!("[x] Too many errors detected, attacks is going to be stopped");
    }

    fn report_attack_summary(&mut self, discovered_users_count: Option<usize>, non_preauth_count: usize) {
        println!("\n  == Execution summary == \n");

        if let Some(discovered_users_count) = discovered_users_count {
            if discovered_users_count > 0 {
                println!("[+] Great, {} user passwords were discovered", discovered_users_count);
            }
            else {
                println!("[-] Unfortunately, no user passwords were discovered");
            }
        }

        if non_preauth_count > 0 {
            println!("[+] {} users without pre-auth required were discovered, good luck on crack", non_preauth_count);            
        }
        else {
            println!("[-] No users without pre-auth required were discovered");
        }

    }
}