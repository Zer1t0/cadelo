use ascii::AsciiString;
use kerbeiros;

pub enum BruterResult {
    ValidPassword(AsciiString, String, kerbeiros::Credential),
    ValidUser(AsciiString),
    NonPreauthUser(AsciiString, kerbeiros::AsRep),
    InvalidUser(AsciiString),
    DisabledUser(AsciiString),
    Error(AsciiString, String, kerbeiros::Error),
    KrbError(AsciiString, String, kerbeiros::KrbError)
}