use clap::*;

pub trait CadeloModule {
    fn name(&self) -> &'static str;
    fn args(&self) -> App<'static, 'static>;
    fn main(&mut self, args: ArgMatches);
}