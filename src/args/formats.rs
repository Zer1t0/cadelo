
use std::fmt;


#[derive(Debug, PartialEq, Clone, Copy)]
pub enum CrackFormat {
    Hashcat,
    John
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum TicketFormat {
    Krb,
    Ccache
}

impl fmt::Display for TicketFormat {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            TicketFormat::Ccache => write!(f, "ccache"),
            TicketFormat::Krb => write!(f, "krb")
        }
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Cipher {
    RC4,
    AES128,
    AES256
}

impl Cipher {

    pub fn etype(&self) -> i32 {
        match self {
            Cipher::RC4 => kerbeiros::RC4_HMAC,
            Cipher::AES128 => kerbeiros::AES128_CTS_HMAC_SHA1_96,
            Cipher::AES256 => kerbeiros::AES256_CTS_HMAC_SHA1_96
        }
    }
 
}