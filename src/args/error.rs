use std::fmt;
use failure::*;
use failure_derive::Fail;


pub type ArgsResult<T> = Result<T, ArgsError>;

#[derive(Debug)]
pub struct ArgsError {
    inner: Context<ArgsErrorKind>
}


#[derive(Clone, PartialEq, Debug, Fail)]
pub enum ArgsErrorKind {
    #[fail (display = "Invalid domain \"{}\", it must be an ascii name", _0)]
    NonAsciiDomain(String),

    #[fail (display = "Invalid KDC IP \"{}\", it must be a valid IP address (for example 1.3.3.7)", _0)]
    NonKdcIP(String),

    #[fail (display = "Unable to resolve the kdc address of realm {}", _0)]
    NoKdcResolution(String),

    #[fail (display = "Threads must be a positive integer bigger than 0")]
    InvalidThreads,

    #[fail (display = "Invalid NTLM string: {}", _0)]
    InvalidNTLM(String),

    #[fail (display = "Invalid AES-128 key string: {}", _0)]
    InvalidAES128Key(String),

    #[fail (display = "Invalid AES-256 key string: {}", _0)]
    InvalidAES256Key(String),

    #[fail (display = "You must provide at least one password in file {}", _0)]
    EmptyPasswordsFile(String),

    #[fail (display = "Invalid username \"{}\", it must be an ascii name", _0)]
    NonAsciiUsername(String),

    #[fail (display = "You must provide at least one username in file {}", _0)]
    EmptyUsernamesFile(String),

    #[fail (display = "Error opening file {} : {}", _0, _1)]
    ErrorOpeningFile(String, String),
}

impl Fail for ArgsError {
    fn cause(&self) -> Option<&dyn Fail> {
        self.inner.cause()
    }

    fn backtrace(&self) -> Option<&Backtrace> {
        self.inner.backtrace()
    }
}

impl fmt::Display for ArgsError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Display::fmt(&self.inner, f)
    }
}

impl From<ArgsErrorKind> for ArgsError {
    fn from(kind: ArgsErrorKind) -> ArgsError {
        return ArgsError {
            inner: Context::new(kind)
        };
    }
}
