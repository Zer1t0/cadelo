use clap::*;
use ascii::AsciiString;
use std::net::IpAddr;
use std::fs::File;
use std::io::{BufReader, BufRead};
use std::str::FromStr;
use std::result::Result;
use std::u8;

use super::error::*;
use super::formats::*;

use kerbeiros;

#[derive(Default)]
pub struct ArgsParser {}


impl ArgsParser {

    pub fn parse_domain(&self, args: &ArgMatches) -> ArgsResult<AsciiString> {
        let domain = args.value_of("domain").unwrap();
        return AsciiString::from_ascii(domain).map_err(|_|
            ArgsError::from(ArgsErrorKind::NonAsciiDomain(domain.to_string()))
        );
    }

    pub fn parse_kdc_ip(&self, args: &ArgMatches, domain: &AsciiString) -> ArgsResult<IpAddr> {
        if let Some(addr) = args.value_of("kdc-ip") {
            let ip_addr = addr.parse::<IpAddr>().map_err(|_|
                ArgsErrorKind::NonKdcIP(addr.to_string())
            )?;

            return Ok(ip_addr);
        }

        return self.resolve_kdc_address(domain);
    }

    #[cfg(not(test))]
    fn resolve_kdc_address(&self, domain: &AsciiString) -> ArgsResult<IpAddr> {
        return kerbeiros::resolve_realm_kdc(domain).map_err(|_| 
            ArgsError::from(ArgsErrorKind::NoKdcResolution(domain.to_string()))
        );
    }

    #[cfg(test)]
    fn resolve_kdc_address(&self, _domain: &AsciiString) -> ArgsResult<IpAddr> {
        return "0.0.0.0".parse().map_err(|_| 
            ArgsError::from(ArgsErrorKind::NoKdcResolution("".to_string()))
        );
    }

    pub fn parse_verbose(&self, args: &ArgMatches) -> u8 {
        return args.occurrences_of("verbose") as u8;
    }

    pub fn parse_threads(&self, args: &ArgMatches) -> ArgsResult<usize> {
        let threads = args.value_of("threads").unwrap();
        let threads = threads.parse::<usize>().map_err(|_|
            ArgsErrorKind::InvalidThreads
        )?;

        if threads == 0 {
            return Err(ArgsErrorKind::InvalidThreads)?;
        }
        return Ok(threads);
    }

    pub fn parse_ticket_format(&self, args: &ArgMatches) -> TicketFormat {
        let format = args.value_of("ticket-format").unwrap().to_lowercase();
        
        match format.as_str() {
            "krb" => TicketFormat::Krb,
            "ccache" => TicketFormat::Ccache,
            _ => panic!()
        }
    }

    pub fn parse_transport_protocol(&self, args: &ArgMatches) -> kerbeiros::TransportProtocol {
        if args.is_present("udp") {
            return kerbeiros::TransportProtocol::UDP
        }

        return kerbeiros::TransportProtocol::TCP;
    }

    pub fn parse_crack_format(&self, args: &ArgMatches) -> CrackFormat {
        let format = args.value_of("crack-format").unwrap().to_lowercase();
        
        match format.as_str() {
            "john" => CrackFormat::John,
            "hashcat" => CrackFormat::Hashcat,
            _ => panic!()
        }
    }

    pub fn parse_password(&self, args: &ArgMatches) -> Option<String> {
        return args.value_of("password").map(|s| s.to_string());
    }

    pub fn parse_ntlm(&self, args: &ArgMatches) -> ArgsResult<Option<kerbeiros::Key>> {
        if let Some(ntlm_str) = args.value_of("ntlm") {
            let key = kerbeiros::Key::from_rc4_key_string(ntlm_str).map_err(|err|
                ArgsErrorKind::InvalidNTLM(format!("{}", err))
            )?;
            return Ok(Some(key));
        }

        return Ok(None);
    }

    pub fn parse_aes_128_key(&self, args: &ArgMatches) -> ArgsResult<Option<kerbeiros::Key>> {
        if let Some(aes_128_key_str) = args.value_of("aes-128") {
            let key = kerbeiros::Key::from_aes_128_key_string(aes_128_key_str).map_err(|err|
                ArgsErrorKind::InvalidAES128Key(format!("{}", err))
            )?;
            return Ok(Some(key));
        }

        return Ok(None);
    }

    pub fn parse_aes_256_key(&self, args: &ArgMatches) -> ArgsResult<Option<kerbeiros::Key>> {
        if let Some(aes_256_key_str) = args.value_of("aes-256") {
            let key = kerbeiros::Key::from_aes_256_key_string(aes_256_key_str).map_err(|err|
                ArgsErrorKind::InvalidAES256Key(format!("{}", err))
            )?;
            return Ok(Some(key));
        }

        return Ok(None);
    }

    pub fn parse_passwords(&self, args: &ArgMatches) -> ArgsResult<Option<Vec<String>>> {
        if let Some(passwords_filepath) = args.value_of("passwords") {
            let passwords = self.get_file_lines(passwords_filepath)?;
            if passwords.len() == 0 {
                return Err(ArgsErrorKind::EmptyPasswordsFile(passwords_filepath.to_string()))?;
            }
            return Ok(Some(passwords));
        } else {
            return Ok(None);
        }
    }

    pub fn parse_username(&self, args: &ArgMatches) -> ArgsResult<Option<AsciiString>> {
        if let Some(username) = args.value_of("username") {
            let username = AsciiString::from_ascii(username).map_err(|_|
                ArgsError::from(ArgsErrorKind::NonAsciiUsername(username.to_string()))
            )?;

            return Ok(Some(username));
        } else {
            return Ok(None);
        }
        
    }

    pub fn parse_usernames(&self, args: &ArgMatches) -> ArgsResult<Option<Vec<AsciiString>>> {
        if let Some(usernames_filepath) = args.value_of("usernames") {
            return self.read_usernames_from_file(usernames_filepath);
        } else {
            return Ok(None);
        }
    }

    fn read_usernames_from_file(&self, usernames_filepath: &str) -> ArgsResult<Option<Vec<AsciiString>>> {
        let usernames_strings = self.get_file_lines(usernames_filepath)?;

        if usernames_strings.len() == 0 {
            return Err(ArgsErrorKind::EmptyUsernamesFile(usernames_filepath.to_string()))?;
        }

        let usernames = self.convert_to_ascii_usernames(&usernames_strings)?;

        return Ok(Some(usernames));
    }

    fn convert_to_ascii_usernames(&self, usernames_strings: &Vec<String>) -> ArgsResult<Vec<AsciiString>> {
        let mut usernames = Vec::with_capacity(usernames_strings.len());
            for username in usernames_strings.iter() {
                usernames.push(
                    AsciiString::from_str(username).map_err(|_|
                        ArgsErrorKind::NonAsciiUsername(username.to_string())
                    )?
                );
            }
        return Ok(usernames);
    }

    fn get_file_lines(&self, filename : &str) -> ArgsResult<Vec<String>> {
        let fd = File::open(filename).map_err(|error|
            ArgsErrorKind::ErrorOpeningFile(filename.to_string(), format!("{}", error))
        )?;
        let file_lines: Vec<String> = BufReader::new(fd).lines().filter_map(Result::ok).collect();

        return Ok(file_lines);
    }

}
