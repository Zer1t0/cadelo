mod brute;
mod asreproast;
mod asktgt;
mod args;
mod module;
mod bruter;

use module::CadeloModule;

use clap::*;


fn modules() -> Vec<Box<dyn CadeloModule>> {
    return vec![
        Box::new(brute::BruteModule::default()),
        Box::new(asktgt::AskTgtModule::default()),
        Box::new(asreproast::AsRepRoastModule::default())
    ]
}

fn args() -> App<'static, 'static> {
    let mods = modules();
    
    let mut app = App::new("cadelo")
        .author("Zer1t0")
        .version(env!("CARGO_PKG_VERSION"))
        .about("Work over Kerberos");

    for m in mods.iter() {
        app = app.subcommand(m.args());
    }

    return app;
}


fn main() {
    let mut app_args = args();
    let matches = app_args.clone().get_matches();

    match matches.subcommand_name() {
        Some(subcommand_name) => {
            execute_module(subcommand_name, matches.clone());
        }
        None => {
            app_args.print_help().unwrap();
            println!("");
        }
    }
}

fn execute_module(name: &str, args: ArgMatches) {
    let mut mods = modules();
    for m in mods.iter_mut() {
        if m.name() == name {
            m.main(args);
            return;
        }
    }
    unreachable!()
}
