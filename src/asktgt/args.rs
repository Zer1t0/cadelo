use ascii::AsciiString;
use std::net::IpAddr;
use clap::*;
use crate::args::*;
use kerbeiros;


pub struct AskTgtOptions {
    pub domain: AsciiString,
    pub username: AsciiString,
    pub user_key: kerbeiros::Key,
    pub output_filename: String,
    pub ticket_format: TicketFormat,
    pub kdc_ip: IpAddr
}

pub struct AskTgtArgsParser<'a> {
    submatches: &'a ArgMatches<'a>,
    args_parser: ArgsParser
}

impl<'a> AskTgtArgsParser<'a> {

    pub fn parse(matches: &'a ArgMatches) -> ArgsResult<AskTgtOptions>  {
        let parser = AskTgtArgsParser {
            submatches: matches.subcommand_matches(super::SUBCOMMAND_NAME).unwrap(),
            args_parser: ArgsParser::default()
        };

        return parser._parse();
    }

    fn _parse(&self) -> ArgsResult<AskTgtOptions> {
        let domain = self.parse_domain()?;
        let username = self.parse_username()?;
        let user_key = self.parse_user_key()?;
        let ticket_format = self.parse_ticket_format();
        let output_filename = self.parse_output_filename(&username, &ticket_format);
        let kdc_ip = self.parse_kdc_ip(&domain)?;

        return Ok(AskTgtOptions{
            domain,
            username,
            user_key,
            output_filename,
            ticket_format,
            kdc_ip
        });
    }

    fn parse_domain(&self) -> ArgsResult<AsciiString> {
        return self.args_parser.parse_domain(self.submatches);
    }

    fn parse_kdc_ip(&self, domain: &AsciiString) -> ArgsResult<IpAddr> {
        return self.args_parser.parse_kdc_ip(self.submatches, domain);
    }

    fn parse_username(&self) -> ArgsResult<AsciiString> {
        return Ok(self.args_parser.parse_username(self.submatches)?.unwrap());
    }

    fn parse_user_key(&self) -> ArgsResult<kerbeiros::Key> {
        if let Some(password) = self.args_parser.parse_password(self.submatches) {
            return Ok(kerbeiros::Key::Password(password.to_string()));
        }
        else if let Some(ntlm) = self.args_parser.parse_ntlm(self.submatches)? {
            return Ok(ntlm);
        }
        else if let Some(aes_128_key) = self.args_parser.parse_aes_128_key(self.submatches)? {
            return Ok(aes_128_key);
        }
        else if let Some(aes_256_key) = self.args_parser.parse_aes_256_key(self.submatches)? {
            return Ok(aes_256_key);
        }
        unreachable!()
    }

    fn parse_ticket_format(&self) -> TicketFormat {
        return self.args_parser.parse_ticket_format(self.submatches);
    }

    fn parse_output_filename(&self, username: &AsciiString, ticket_format: &TicketFormat) -> String {
        if let Some(output_filename) = self.submatches.value_of("out-file") {
            return output_filename.to_string();
        }
        
        return format!("{}.{}", username, ticket_format);
    }

}

#[cfg(test)]
mod test {
    use super::*;
    use std::net::*;

    fn parse_args(mut args: Vec<&str>) -> ArgsResult<AskTgtOptions> {

        let mut complete_args = vec!["program", "asktgt"];
        complete_args.append(&mut args);

        let args_definition = crate::args();
        let matches = args_definition.get_matches_from_safe(complete_args).unwrap();
        return AskTgtArgsParser::parse(&matches);
    }

    #[test]
    fn provide_domain() {
        let options = parse_args(vec!["--username", "user", "--password", "pass", "--domain", "dom"]).unwrap();
        assert_eq!("dom", options.domain);
    }

    #[should_panic(expected="MissingRequiredArgument")]
    #[test]
    fn no_provide_domain() {
        parse_args(vec![]).unwrap();
    }

    #[should_panic(expected="Invalid domain \"doñ\", it must be an ascii name")]
    #[test]
    fn provide_non_ascii_domain() {
        parse_args(vec!["--username", "user", "--password", "pass", "--domain", "doñ"]).unwrap();
    }

    #[test]
    fn provide_username() {
        let options = parse_args(vec!["--username", "user", "--password", "pass", "--domain", "dom"]).unwrap();
        assert_eq!("user", options.username);
    }

    #[should_panic(expected="MissingRequiredArgument")]
    #[test]
    fn no_provide_username() {
        parse_args(vec!["--password", "pass", "--domain", "dom"]).unwrap();
    }

    #[should_panic(expected="Invalid username \"ñ\", it must be an ascii name")]
    #[test]
    fn provide_non_ascii_username() {
        parse_args(vec!["--username", "ñ", "--password", "pass", "--domain", "dom"]).unwrap();
    }

    #[test]
    fn provide_password() {
        let options = parse_args(vec!["--username", "user", "--password", "pass", "--domain", "dom"]).unwrap();
        assert_eq!(kerbeiros::Key::Password("pass".to_string()), options.user_key);
    }

    #[should_panic(expected="MissingRequiredArgument")]
    #[test]
    fn no_provide_user_key() {
        parse_args(vec!["--username", "user", "--domain", "dom"]).unwrap();
    }


    #[test]
    fn provide_ntlm() {
        let options = parse_args(vec!["--username", "user", "--ntlm", "0123456789ABCDEF0123456789abcdef", "--domain", "dom"]).unwrap();
        assert_eq!(
            kerbeiros::Key::RC4Key(
                [0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef]
            ),
            options.user_key);
    }

    #[should_panic(expected="Invalid NTLM string: Invalid key: Length should be 32")]
    #[test]
    fn provide_ntlm_incorrect_length() {
        parse_args(vec!["--username", "user", "--ntlm", "0", "--domain", "dom"]).unwrap();
    }

    #[should_panic(expected="Invalid NTLM string: Invalid key: Only hexadecimal characters are allowed [1234567890abcdefABCDEF]")]
    #[test]
    fn provide_ntlm_non_hex() {
        parse_args(vec!["--username", "user", "--ntlm", "XXX_456789ABCDEF0123456789abcdef", "--domain", "dom"]).unwrap();
    }

    #[test]
    fn provide_aes_128() {
        let options = parse_args(vec!["--username", "user", "--aes-128", "0123456789ABCDEF0123456789abcdef", "--domain", "dom"]).unwrap();
        assert_eq!(
            kerbeiros::Key::AES128Key(
                [0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef]
            ),
            options.user_key);
    }


    #[should_panic(expected="Invalid AES-128 key string: Invalid key: Length should be 32")]
    #[test]
    fn provide_aes_128_incorrect_length() {
        parse_args(vec!["--username", "user", "--aes-128", "0", "--domain", "dom"]).unwrap();
    }

    #[should_panic(expected="Invalid AES-128 key string: Invalid key: Only hexadecimal characters are allowed [1234567890abcdefABCDEF]")]
    #[test]
    fn provide_aes_128_non_hex() {
        parse_args(vec!["--username", "user", "--aes-128", "XXX_456789ABCDEF0123456789abcdef", "--domain", "dom"]).unwrap();
    }

    #[test]
    fn provide_aes_256() {
        let options = parse_args(
            vec!["--username", "user", 
            "--aes-256", "0123456789ABCDEF0123456789abcdef0123456789ABCDEF0123456789abcdef", "--domain", "dom"]
        ).unwrap();
        assert_eq!(
            kerbeiros::Key::AES256Key(
                [0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 
                0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef]
            ),
            options.user_key);
    }

    #[should_panic(expected="Invalid AES-256 key string: Invalid key: Length should be 64")]
    #[test]
    fn provide_aes_256_incorrect_length() {
        parse_args(vec!["--username", "user", "--aes-256", "0", "--domain", "dom"]).unwrap();
    }

    #[should_panic(expected="Invalid AES-256 key string: Invalid key: Only hexadecimal characters are allowed [1234567890abcdefABCDEF]")]
    #[test]
    fn provide_aes_256_non_hex() {
        parse_args(
            vec!["--username", "user", 
            "--aes-256", "XXX_456789ABCDEF0123456789abcdef0123456789ABCDEF0123456789abcdef", "--domain", "dom"]
        ).unwrap();
    }

    #[test]
    fn provide_ticket_format_ccache() {
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
            "--ticket-format", "ccache"
        ]).unwrap();

        assert_eq!(TicketFormat::Ccache, options.ticket_format);
    }

    #[test]
    fn provide_ticket_format_krb() {
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
            "--ticket-format", "krb"
        ]).unwrap();

        assert_eq!(TicketFormat::Krb, options.ticket_format);
    }

    #[should_panic(expected="InvalidValue")]
    #[test]
    fn provide_ticket_format_unknown() {
        parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
            "--ticket-format", "unknown"
        ]).unwrap();
    }


    #[test]
    fn provide_output_file() {
        let filename = "ticket.file";
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
            "--out-file", filename
        ]).unwrap();

        assert_eq!(filename, options.output_filename);
    }

    #[test]
    fn no_provide_output_file() {
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom"
        ]).unwrap();

        assert_eq!("user.ccache", options.output_filename);
    }

    #[test]
    fn no_provide_output_file_and_ticket_ccache() {
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
            "--ticket-format", "ccache"
        ]).unwrap();

        assert_eq!("user.ccache", options.output_filename);
    }

    #[test]
    fn no_provide_output_file_and_ticket_krb() {
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
            "--ticket-format", "krb"
        ]).unwrap();

        assert_eq!("user.krb", options.output_filename);
    }

    #[test]
    fn provide_kdc_ip() {
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
            "--kdc-ip", "1.3.3.7"
        ]).unwrap();

        assert_eq!(IpAddr::V4(Ipv4Addr::new(1, 3, 3, 7)), options.kdc_ip);
    }

    #[test]
    fn no_provide_kdc_ip() {
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom"
        ]).unwrap();

        assert_eq!(IpAddr::V4(Ipv4Addr::new(0, 0, 0, 0)), options.kdc_ip);
    }

    #[should_panic(expected="Invalid KDC IP \"noip\", it must be a valid IP address (for example 1.3.3.7)")]
    #[test]
    fn provide_incorrect_kdc_ip() {
        parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
            "--kdc-ip", "noip"
        ]).unwrap();
    }

}