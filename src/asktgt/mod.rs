mod args;
mod executor;

use clap::*;
use args::*;
use executor::AskTgtExecutor;


use crate::module::CadeloModule;

const SUBCOMMAND_NAME: &str = "asktgt";

#[derive(Default)]
pub struct AskTgtModule {
}

impl CadeloModule for AskTgtModule {

    fn name(&self) -> &'static str {
        return SUBCOMMAND_NAME;
    }

    fn args(&self) -> App<'static, 'static> {
        SubCommand::with_name(self.name())
        .about("Retrieve a TGT(Ticket Granting Ticket)")
        .author("Zer1t0")
        .about("Get a Kerberos TGT")
        .arg(
            Arg::with_name("domain")
                .long("domain")
                .short("d")
                .takes_value(true)
                .help("Domain to brute-force")
                .required(true)
        )
        .arg(
            Arg::with_name("username")
                .long("username")
                .short("u")
                .takes_value(true)
                .help("Username what TGT is for")
                .required(true)
        )
        .arg(
            Arg::with_name("password")
                .long("password")
                .short("p")
                .takes_value(true)
                .help("Password of user")
        )
        .arg(
            Arg::with_name("ntlm")
                .long("ntlm")
                .takes_value(true)
                .help("NTLM hash of user")
        )
        .arg(
            Arg::with_name("aes-128")
                .long("aes-128")
                .takes_value(true)
                .help("AES 128 Kerberos key of user")
        )
        .arg(
            Arg::with_name("aes-256")
                .long("aes-256")
                .takes_value(true)
                .help("AES 256 Kerberos key of user")
        )
        .group(
            ArgGroup::with_name("user_key")
            .args(&["password", "ntlm", "aes-128", "aes-256"])
            .multiple(false)
            .required(true)
         )
        .arg(
            Arg::with_name("kdc-ip")
                .long("kdc-ip")
                .short("k")
                .value_name("ip")
                .takes_value(true)
                .help("The address of the KDC")
        )
        .arg(
            Arg::with_name("ticket-format")
                .long("ticket-format")
                .takes_value(true)
                .possible_values(&["krb", "ccache"])
                .help("Format to save retrieved tickets.")
                .default_value("ccache")
        )
        .arg(
            Arg::with_name("out-file")
                .long("out-file")
                .takes_value(true)
                .value_name("file")
                .help("File to save TGT.")
        )
    }

    fn main(&mut self, args: ArgMatches) {
        let options;
        match AskTgtArgsParser::parse(&args) {
            Ok(opts) => {
                options = opts
            },
            Err(error) => {
                println!("[X] {}", error);
                return;
            }
        }

        AskTgtExecutor::execute(options);
    }

}

