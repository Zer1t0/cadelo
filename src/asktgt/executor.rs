use std::net::IpAddr;
use ascii::AsciiString;
use super::args::AskTgtOptions;
use crate::args::TicketFormat;

use kerbeiros;

pub struct AskTgtExecutor {
    options: AskTgtOptions
}

impl AskTgtExecutor {

    pub fn execute(options: AskTgtOptions) {
        let executor = Self{options};
        executor.request_tgt()
    }

    fn request_tgt(&self) {
        let requester = kerbeiros::TgtRequester::new(self.domain().clone(), self.kdc_ip().clone());

        match requester.request(self.username(), Some(self.user_key())) {
            Ok(credential) => {
               self.save_credential(credential); 
            }
            Err(error) => {
                self.handle_request_error(error);
            }
        }
    }

    fn save_credential(&self, credential: kerbeiros::Credential) {
        let save_result = match self.ticket_format() {
            TicketFormat::Ccache => credential.save_into_ccache_file(self.output_filename()),
            TicketFormat::Krb => credential.save_into_krb_cred_file(self.output_filename())
        };

        match save_result {
            Ok(_) => {
                println!("[+] Saved TGT into {}", self.output_filename());
            }
            Err(error) => {
                println!("[X] Error saving TGT into {} : {}", self.output_filename(), error);
            }
        }
    }

    fn handle_request_error(&self, error: kerbeiros::Error) {
        println!("[X] Error requesting TGT: {}", error);
    }

    fn user_key(&self) -> &kerbeiros::Key {
        return &self.options.user_key;
    }

    fn username(&self) -> &AsciiString {
        return &self.options.username;
    }

    fn domain(&self) -> &AsciiString {
        return &self.options.domain;
    }

    fn kdc_ip(&self) -> &IpAddr {
        return &self.options.kdc_ip;
    }

    fn output_filename(&self) -> &String {
        return &self.options.output_filename;
    }

    fn ticket_format(&self) -> &TicketFormat {
        return &self.options.ticket_format;
    }

}