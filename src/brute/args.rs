use crate::args::*;

use clap::*;
use ascii::AsciiString;
use std::net::IpAddr;

pub struct BruteOptions {
    pub domain: AsciiString,
    pub kdc_ip: IpAddr,
    pub usernames: Vec<AsciiString>,
    pub passwords: Vec<String>,
    pub output_passwords: Option<String>,
    pub ticket_format: Option<TicketFormat>,
    pub crack_format: CrackFormat,
    pub output_non_preauth: Option<String>,
    pub verbosity: u8,
    pub transport_protocol: kerbeiros::TransportProtocol,
    pub threads: usize,
}

pub struct BruteArgsParser<'a> {
    submatches: &'a ArgMatches<'a>,
    args_parser: ArgsParser
}


impl<'a> BruteArgsParser<'a> {

    pub fn parse(matches: &'a ArgMatches) -> ArgsResult<BruteOptions>  {
        let parser = BruteArgsParser {
            submatches: matches.subcommand_matches(super::SUBCOMMAND_NAME).unwrap(),
            args_parser: ArgsParser::default()
        };

        return parser._parse();
    }

    fn _parse(&self) -> ArgsResult<BruteOptions> {
        let domain = self.parse_domain()?;
        let kdc_ip = self.parse_kdc_ip(&domain)?;

        return Ok(BruteOptions{
            domain,
            kdc_ip,
            usernames: self.parse_usernames()?,
            passwords: self.parse_passwords()?,
            output_passwords: self.parse_output_passwords(),
            ticket_format: self.parse_ticket_format()?,
            crack_format: self.parse_crack_format(),
            output_non_preauth: self.parse_output_non_preauth(),
            verbosity: self.parse_verbosity(),
            transport_protocol: self.parse_use_udp(),
            threads: self.parse_threads()?
        });
    } 

    fn parse_domain(&self) -> ArgsResult<AsciiString> {
        return self.args_parser.parse_domain(self.submatches);
    }

    fn parse_kdc_ip(&self, domain: &AsciiString) -> ArgsResult<IpAddr> {
        return self.args_parser.parse_kdc_ip(self.submatches, domain);
    }

    fn parse_usernames(&self) -> ArgsResult<Vec<AsciiString>> {
        if let Some(usernames) = self.args_parser.parse_usernames(self.submatches)? {
            return Ok(usernames);
        } 
        else if let Some(username) = self.args_parser.parse_username(self.submatches)? {
                return Ok(vec![username]); 
        } 
        unreachable!()        
    }

    fn parse_passwords(&self) -> ArgsResult<Vec<String>> {
        if let Some(passwords) = self.args_parser.parse_passwords(self.submatches)? {
            return Ok(passwords);
        } else if let Some(password) = self.args_parser.parse_password(self.submatches) {
            return Ok(vec![password]);
        } 
        unreachable!()
    }

    fn parse_output_passwords(&self) -> Option<String>  {
        return self.submatches.value_of("output-passwords").map(|s| s.to_string());
    }

    fn parse_ticket_format(&self) -> ArgsResult<Option<TicketFormat>> {
        let save_ticket = !self.submatches.is_present("no-save-ticket");
        if !save_ticket {
            return Ok(None);
        }

        return Ok(Some(self.args_parser.parse_ticket_format(self.submatches)));
    }

    fn parse_crack_format(&self) -> CrackFormat {
        return self.args_parser.parse_crack_format(self.submatches);
    }

    fn parse_output_non_preauth(&self) -> Option<String>  {
        return self.submatches.value_of("output-non-preauth").map(|s| s.to_string());
    }

    fn parse_verbosity(&self) -> u8 {
        return self.args_parser.parse_verbose(self.submatches);
    }

    fn parse_use_udp(&self) -> kerbeiros::TransportProtocol {
        return self.args_parser.parse_transport_protocol(self.submatches);
    }

    fn parse_threads(&self) -> ArgsResult<usize> {
        return self.args_parser.parse_threads(self.submatches);
    }

}


#[cfg(test)]
mod test {
    use super::*;
    use std::net::*;

    fn resources_path() -> String {
        return format!("{}/{}",env!("CARGO_MANIFEST_DIR"),"test_resources");
    }

    fn parse_args(mut args: Vec<&str>) -> ArgsResult<BruteOptions> {

        let mut complete_args = vec!["program", "brute"];
        complete_args.append(&mut args);

        let args_definition = crate::args();
        let matches = args_definition.get_matches_from_safe(complete_args).unwrap();
        return BruteArgsParser::parse(&matches);
    }

    #[test]
    fn provide_domain() {
        let options = parse_args(vec!["--username", "user", "--password", "pass", "--domain", "dom"]).unwrap();
        assert_eq!("dom", options.domain);
    }

    #[should_panic(expected="MissingRequiredArgument")]
    #[test]
    fn no_provide_domain() {
        parse_args(vec![]).unwrap();
    }

    #[should_panic(expected="Invalid domain \"doñ\", it must be an ascii name")]
    #[test]
    fn provide_non_ascii_domain() {
        parse_args(vec!["--username", "user", "--password", "pass", "--domain", "doñ"]).unwrap();
    }

    #[test]
    fn provide_username() {
        
        let options = parse_args(vec!["--username", "user", "--password", "pass", "--domain", "dom"]).unwrap();

        assert_eq!(vec!["user"], options.usernames);
    }

    #[should_panic(expected="Invalid username \"ñ\", it must be an ascii name")]
    #[test]
    fn provide_non_ascii_username() {
        parse_args(vec!["--username", "ñ", "--password", "pass", "--domain", "dom"]).unwrap();
    }

    #[test]
    fn provide_password() {
        let options = parse_args(vec!["--username", "user", "--password", "pass", "--domain", "dom"]).unwrap();
        assert_eq!(vec!["pass"], options.passwords);
    }


    #[test]
    fn provide_usernames() {
        let usernames_filepath = resources_path() + "/valid_usernames.txt";
        let options = parse_args(vec!["--usernames", &usernames_filepath, "--password", "pass", "--domain", "dom"]).unwrap();

        assert_eq!(vec!["user1", "user2", "user3"], options.usernames);
    }

    #[should_panic(expected="MissingRequiredArgument")]
    #[test]
    fn no_provide_any_username() {
        parse_args(vec!["--domain", "d"]).unwrap();
    }

    #[should_panic(expected="Invalid username \"userñ\", it must be an ascii name")]
    #[test]
    fn provide_non_ascii_usernames() {
        let usernames_filepath = resources_path() + "/non_ascii_usernames.txt";
        parse_args(vec!["--usernames", &usernames_filepath, "--password", "pass", "--domain", "dom"]).unwrap();
    }


    #[should_panic(expected="Error opening file")]
    #[test]
    fn provide_non_existent_usernames_file() {
        let usernames_filepath = resources_path() + "/non_existent_file.txt";
        parse_args(vec!["--usernames", &usernames_filepath, "--password", "pass", "--domain", "dom"]).unwrap();
    }

    #[should_panic(expected="You must provide at least one username in file")]
    #[test]
    fn provide_usernames_file_without_content() {
        let usernames_filepath = resources_path() + "/empty.txt";
        parse_args(vec!["--usernames", &usernames_filepath, "--password", "pass", "--domain", "dom"]).unwrap();
    }

    #[test]
    fn provide_passwords() {
        let passwords_filepath = resources_path() + "/valid_passwords.txt";
        let options = parse_args(vec!["--username", "user", "--passwords", &passwords_filepath, "--domain", "dom"]).unwrap();

        assert_eq!(vec!["password1", "password2"], options.passwords);
    }

    #[should_panic(expected="MissingRequiredArgument")]
    #[test]
    fn no_provide_any_password() {
        parse_args(vec!["--domain", "d", "--username", "user"]).unwrap();
    }

    #[should_panic(expected="You must provide at least one password in file")]
    #[test]
    fn provide_passwords_file_without_content() {
        let passwords_filepath = resources_path() + "/empty.txt";
        parse_args(vec!["--username", "user", "--passwords", &passwords_filepath, "--domain", "dom"]).unwrap();
    }

    #[should_panic(expected="Error opening file")]
    #[test]
    fn provide_non_existent_passwords_file() {
        let passwords_filepath = resources_path() + "/non_existent_file.txt";
        parse_args(vec!["--username", "user", "--passwords", &passwords_filepath, "--domain", "dom"]).unwrap();
    }


    #[test]
    fn provide_kdc_ip() {
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
            "--kdc-ip", "1.3.3.7"
        ]).unwrap();

        assert_eq!(IpAddr::V4(Ipv4Addr::new(1, 3, 3, 7)), options.kdc_ip);
    }

    #[should_panic(expected="Invalid KDC IP \"noip\", it must be a valid IP address (for example 1.3.3.7)")]
    #[test]
    fn provide_incorrect_kdc_ip() {
        parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
            "--kdc-ip", "noip"
        ]).unwrap();
    }

    #[test]
    fn no_provide_output_passwords() {
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
        ]).unwrap();

        assert_eq!(None, options.output_passwords);
    }

    #[test]
    fn provide_output_passwords() {
        let output_passwords = "passwords.txt";
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
            "--output-passwords", output_passwords
        ]).unwrap();

        assert_eq!(Some(output_passwords.to_string()), options.output_passwords);
    }


    #[test]
    fn no_provide_ticket_format() {
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
        ]).unwrap();

        assert_eq!(Some(TicketFormat::Ccache), options.ticket_format);
    }


    #[test]
    fn provide_ticket_format_ccache() {
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
            "--ticket-format", "ccache"
        ]).unwrap();

        assert_eq!(Some(TicketFormat::Ccache), options.ticket_format);
    }

    #[test]
    fn provide_ticket_format_krb() {
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
            "--ticket-format", "krb"
        ]).unwrap();

        assert_eq!(Some(TicketFormat::Krb), options.ticket_format);
    }

    #[should_panic(expected="InvalidValue")]
    #[test]
    fn provide_ticket_format_unknown() {
        parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
            "--ticket-format", "unknown"
        ]).unwrap();
    }

    #[test]
    fn provide_no_save_ticket() {
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
            "--no-save-ticket"
        ]).unwrap();

        assert_eq!(None, options.ticket_format);
    }


    #[test]
    fn no_provide_preauth_format() {
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
        ]).unwrap();

        assert_eq!(CrackFormat::Hashcat, options.crack_format);
    }

    #[test]
    fn provide_preauth_format_hashcat() {
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
            "--crack-format", "hashcat"
        ]).unwrap();

        assert_eq!(CrackFormat::Hashcat, options.crack_format);
    }

    #[test]
    fn provide_preauth_format_john() {
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
            "--crack-format", "john"
        ]).unwrap();

        assert_eq!(CrackFormat::John, options.crack_format);
    }

    #[should_panic(expected="InvalidValue")]
    #[test]
    fn provide_preauth_format_unknown() {
        parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
            "--crack-format", "unknown"
        ]).unwrap();
    }

    #[test]
    fn provide_output_non_preauth() {
        let non_preauth_file = "aaa.txt";
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
            "--output-non-preauth", &non_preauth_file
        ]).unwrap();

        assert_eq!(Some(non_preauth_file.to_string()), options.output_non_preauth);
    }

    #[test]
    fn no_provide_output_non_preauth() {
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
        ]).unwrap();

        assert_eq!(None, options.output_non_preauth);
    }

    #[test]
    fn no_provide_verbose() {
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
        ]).unwrap();

        assert_eq!(0, options.verbosity);
    }

    #[test]
    fn provide_verbose() {
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
            "-v"
        ]).unwrap();

        assert_eq!(1, options.verbosity);
    }


    #[test]
    fn provide_multiple_verbose() {
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
            "-vvv"
        ]).unwrap();

        assert_eq!(3, options.verbosity);
    }

    #[test]
    fn no_provide_threads() {
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
        ]).unwrap();

        assert_eq!(1, options.threads);
    }


    #[test]
    fn provide_threads() {
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
            "--threads", "4"
        ]).unwrap();

        assert_eq!(4, options.threads);
    }


    #[should_panic(expected="Threads must be a positive integer bigger than 0")]
    #[test]
    fn provide_non_number_threads() {
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
            "--threads", "aaaa"
        ]).unwrap();

        assert_eq!(4, options.threads);
    }


    #[should_panic(expected="Threads must be a positive integer bigger than 0")]
    #[test]
    fn provide_0_threads() {
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
            "--threads", "0"
        ]).unwrap();

        assert_eq!(4, options.threads);
    }

    #[test]
    fn no_provide_use_udp() {
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
        ]).unwrap();

        assert_eq!(kerbeiros::TransportProtocol::TCP, options.transport_protocol);
    }

    #[test]
    fn provide_use_udp() {
        let options = parse_args(vec![
            "--username", "user", "--password", "pass", "--domain", "dom",
            "--udp"
        ]).unwrap();

        assert_eq!(kerbeiros::TransportProtocol::UDP, options.transport_protocol);
    }


}