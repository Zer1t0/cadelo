mod args;
mod bruter;
use args::*;
use std::fs::File;

use clap::*;

use crate::module::CadeloModule;
use crate::bruter::*;

const SUBCOMMAND_NAME: &str = "brute";

#[derive(Default)]
pub struct BruteModule {
}

impl CadeloModule for BruteModule {

    fn name(&self) -> &'static str {
        return SUBCOMMAND_NAME;
    }

    fn args(&self) -> App<'static, 'static> {
        SubCommand::with_name(self.name())
        .about("Bruteforce attack")
        .author("Zer1t0")
        .arg(
            Arg::with_name("domain")
                .long("domain")
                .short("d")
                .takes_value(true)
                .help("Domain to brute-force")
                .required(true)
        )
        .arg(
            Arg::with_name("username")
                .long("username")
                .short("u")
                .takes_value(true)
                .help("Username to brute-force")
        )
        .arg(
            Arg::with_name("usernames")
                .long("usernames")
                .takes_value(true)
                .value_name("file")
                .help("File with username per line")
        )
        .group(
            ArgGroup::with_name("users")
            .args(&["username", "usernames"])
            .multiple(false)
            .required(true)
         )
        .arg(
            Arg::with_name("password")
                .long("password")
                .short("p")
                .takes_value(true)
                .help("Password to brute-force")
        )
        .arg(
            Arg::with_name("passwords")
                .long("passwords")
                .takes_value(true)
                .value_name("file")
                .help("File with password per line")
        )
        .group(
            ArgGroup::with_name("pass")
            .args(&["password", "passwords"])
            .multiple(false)
            .required(true)
         )
        .arg(
            Arg::with_name("verbose")
                .long("verbose")
                .short("-v")
                .multiple(true)
                .help("Verbose")
        )
        .arg(
            Arg::with_name("udp")
                .long("udp")
                .help("Use of UDP instead of TCP")
        )
        .arg(
            Arg::with_name("kdc-ip")
                .long("kdc-ip")
                .short("k")
                .value_name("ip")
                .takes_value(true)
                .help("The address of the KDC")
        )
        .arg(
            Arg::with_name("threads")
                .long("threads")
                .short("t")
                .takes_value(true)
                .help("Number of threads")
                .default_value("1")
        )
        .arg(
            Arg::with_name("output-passwords")
                .long("output-passwords")
                .takes_value(true)
                .value_name("file")
                .help("File to save discovered user:password per line")
        )
        .arg(
            Arg::with_name("output-non-preauth")
                .long("output-non-preauth")
                .takes_value(true)
                .value_name("file")
                .help("File to save discovered non preauth responses. It is in hashcat or john format.")
        )
        .arg(
            Arg::with_name("crack-format")
                .long("crack-format")
                .takes_value(true)
                .possible_values(&["hashcat", "john"])
                .help("Format to save non preauth responses.")
                .default_value("hashcat")
        )
        .arg(
            Arg::with_name("no-save-ticket")
                .long("no-save-ticket")
                .help("Do saved the retrieved tickets")
        )
        .arg(
            Arg::with_name("ticket-format")
                .long("ticket-format")
                .takes_value(true)
                .possible_values(&["krb", "ccache"])
                .help("Format to save retrieved tickets.")
                .default_value("ccache")
        )
    }

    fn main(&mut self, args: ArgMatches) {
        let options;
        match BruteArgsParser::parse(&args) {
            Ok(opts) => {
                options = opts
            },
            Err(error) => {
                println!("[X] {}", error);
                return;
            }
        }

        let passwords = options.passwords;
        let usernames = options.usernames;
        let threads_count = options.threads;
        let realm = options.domain;
        let kdc_ip = options.kdc_ip;

        let mut non_preauth_file = None;
        if let Some(non_preauth_filename) = &options.output_non_preauth {
            match File::create(non_preauth_filename) {
                Ok(file) => {
                    non_preauth_file = Some(file);
                }
                Err(error) => {
                    println!("[X] Error creating file {} : {}", non_preauth_filename, error);
                    return;
                }
            }
        }
        
        let reporter = PrintReporter::new(
            options.ticket_format, options.crack_format, non_preauth_file, options.verbosity
        );

        bruter::Bruter::execute(
            usernames, passwords, threads_count, realm, kdc_ip, options.transport_protocol,
            Box::new(reporter));
    }

}
