use std::sync::*;
use ascii::AsciiString;
use kerbeiros;
use crate::bruter::*;

pub struct ResultsHandler {
    invalid_usernames_lock: Arc<RwLock<Vec<AsciiString>>>,
    discovered_users_lock: Arc<RwLock<Vec<AsciiString>>>,
    valid_usernames: Vec<AsciiString>,
    preauth_usernames: Vec<AsciiString>,
    error_count: usize,
    reporter: Box<dyn Reporter>,
    stop_the_attack: Arc<RwLock<bool>>
}

impl ResultsHandler {
    
    pub fn execute(
        receiver: mpsc::Receiver<BruterResult>, 
        invalid_usernames_lock: Arc<RwLock<Vec<AsciiString>>>,
        discovered_users_lock: Arc<RwLock<Vec<AsciiString>>>,
        reporter: Box<dyn Reporter>,
        stop_the_attack: Arc<RwLock<bool>>
    ) {
        let mut collector = Self::new(
            invalid_usernames_lock, discovered_users_lock, 
            reporter, stop_the_attack
        );
        collector.handle_results(receiver);
        collector.close_execution();
    }

    fn new( 
        invalid_usernames_lock: Arc<RwLock<Vec<AsciiString>>>,
        discovered_users_lock: Arc<RwLock<Vec<AsciiString>>>,
        reporter: Box<dyn Reporter>,
        stop_the_attack: Arc<RwLock<bool>>
    ) -> Self {
        return Self{
            invalid_usernames_lock,
            discovered_users_lock,
            valid_usernames: Vec::new(),
            preauth_usernames: Vec::new(),
            error_count: 0,
            reporter,
            stop_the_attack
        };
    }

    fn handle_results(&mut self, receiver: mpsc::Receiver<BruterResult>) {
        for result in receiver {
            self.handle_result(result);
        }
    }

    fn close_execution(&mut self) {
        let discovered_users_count = self.count_discovered_users();
        let non_preauth_count = self.count_non_preauth();
        self.reporter.report_attack_summary(Some(discovered_users_count), non_preauth_count);
    }

    fn count_discovered_users(&self) -> usize {
        let discovered_users = self.discovered_users_lock.read().unwrap();
        return discovered_users.len();
    }

    fn count_non_preauth(&self) -> usize {
        return self.preauth_usernames.len();
    }

    fn handle_result(&mut self, result: BruterResult) {
        match result {
            BruterResult::InvalidUser(username) => {
                self.handle_invalid_username(username);
            },
            BruterResult::DisabledUser(username) => {
                self.handle_disabled_user(username);
            }
            BruterResult::NonPreauthUser(username, asrep) => {
                self.handle_non_preauth_user(username, asrep);
            }
            BruterResult::ValidUser(username) => {
                self.handle_valid_user(username);
            },
            BruterResult::ValidPassword(username, password, credential) => {
                self.handle_valid_password(username, password, credential);
            }
            BruterResult::KrbError(username, password, krb_error) => {
                self.handle_krb_error(username, password, krb_error);
            }
            BruterResult::Error(username, password, error) => {
                self.handle_error(username, password, error);
            }
        }
    }

    fn handle_invalid_username(&mut self, username: AsciiString) {
        self.reset_error_count();
        let mut invalid_usernames = self.invalid_usernames_lock.write().unwrap();
        if !invalid_usernames.contains(&username) {
            self.reporter.report_invalid_username(&username);
            invalid_usernames.push(username.clone());
        }
    }

    fn handle_disabled_user(&mut self, username: AsciiString) {
        self.reset_error_count();
        let mut invalid_usernames = self.invalid_usernames_lock.write().unwrap();
        if !invalid_usernames.contains(&username) {
            self.reporter.report_disabled_user(&username);
            invalid_usernames.push(username.clone());
        }
    }

    fn handle_non_preauth_user(&mut self, username: AsciiString, asrep: kerbeiros::AsRep) {
        self.reset_error_count();
        if !self.preauth_usernames.contains(&username) {
            self.reporter.report_non_preauth_user(&username, &asrep);
            self.valid_usernames.push(username.clone());
            self.preauth_usernames.push(username);
        }
    }

    fn handle_valid_user(&mut self, username: AsciiString) {
        self.reset_error_count();
        if !self.valid_usernames.contains(&username) {
            self.reporter.report_valid_username(&username);
            self.valid_usernames.push(username);
        }
    }

    fn handle_valid_password(&mut self, username: AsciiString, password: String, credential: kerbeiros::Credential) {
        self.reset_error_count();
        let mut discovered_usernames = self.discovered_users_lock.write().unwrap();
        self.reporter.report_valid_username_password(&username, &password, &credential);
        discovered_usernames.push(username);
    }

    fn handle_krb_error(&mut self, username: AsciiString, password: String, krb_error: kerbeiros::KrbError) {
        self.reporter.report_krb_error(&username, &password, &krb_error);
        self.increase_and_check_error_count();
    }

    fn handle_error(&mut self, username: AsciiString, password: String, error: kerbeiros::Error) {
        self.reporter.report_error(&username, &password, &error);
        self.increase_and_check_error_count();
    }

    fn increase_and_check_error_count(&mut self) {
        self.increase_error_count();
        if self.are_there_too_many_errors() {
            let mut stop_the_attack = self.stop_the_attack.write().unwrap();
            *stop_the_attack = true;
            self.reporter.report_attack_stopped_by_errors();
        }
    }

    fn reset_error_count(&mut self) {
        self.error_count = 0;
    }

    fn increase_error_count(&mut self) {
        self.error_count += 1;
    }

    fn are_there_too_many_errors(&self) -> bool {
        return self.error_count > 5;
    }

}
