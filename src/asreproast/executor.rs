use super::args::AsRepRoastOptions;
use std::fs::File;

use super::bruter::*;
use crate::bruter::*;

pub struct AsRepExecutor {
}

impl AsRepExecutor {

    pub fn execute(options: AsRepRoastOptions) {

        let mut output_file = None;
        if let Some(non_preauth_filename) = &options.output_file {
            match File::create(non_preauth_filename) {
                Ok(file) => {
                    output_file = Some(file);
                }
                Err(error) => {
                    println!("[X] Error creating file {} : {}", non_preauth_filename, error);
                    return;
                }
            }
        }

        let reporter = PrintReporter::new(
            None, options.crack_format, output_file, options.verbosity
        );

        Bruter::execute(
            options.usernames, 
            options.threads, 
            options.domain,
            options.kdc_ip, 
            options.cipher,
            options.transport_protocol,
            Box::new(reporter))
    }
}