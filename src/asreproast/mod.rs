mod args;
mod executor;
mod bruter;

use args::*;
use executor::AsRepExecutor;
use clap::*;
use crate::module::CadeloModule;

const SUBCOMMAND_NAME: &str = "asreproast";


#[derive(Default)]
pub struct AsRepRoastModule {}

impl CadeloModule for AsRepRoastModule {

    fn name(&self) -> &'static str {
        return SUBCOMMAND_NAME;
    }

    fn args(&self) -> App<'static, 'static> {
        SubCommand::with_name(self.name())
        .about("AS-REP roast attack")
        .arg(
            Arg::with_name("domain")
                .long("domain")
                .short("d")
                .takes_value(true)
                .help("Domain to brute-force")
                .required(true)
        )
        .arg(
            Arg::with_name("username")
                .long("username")
                .short("u")
                .takes_value(true)
                .help("User to check")
        )
        .arg(
            Arg::with_name("usernames")
                .long("usernames")
                .value_name("file")
                .takes_value(true)
                .help("File with username per line")
        )
        .group(
            ArgGroup::with_name("users")
            .args(&["username", "usernames"])
            .multiple(false)
            .required(true)
         )
         .arg(
            Arg::with_name("kdc-ip")
                .long("kdc-ip")
                .short("k")
                .value_name("ip")
                .takes_value(true)
                .help("The address of the KDC")
        )
        .arg(
            Arg::with_name("threads")
                .long("threads")
                .short("t")
                .takes_value(true)
                .help("Number of threads")
                .default_value("1")
        )
        .arg(
            Arg::with_name("crack-format")
                .long("crack-format")
                .takes_value(true)
                .possible_values(&["hashcat", "john"])
                .help("Format to save non preauth responses.")
                .default_value("hashcat")
        )
        .arg(
            Arg::with_name("out-file")
                .long("out-file")
                .value_name("file")
                .takes_value(true)
                .help("File to save discovered non preauth responses. It is in hashcat or john format.")
        )
        .arg(
            Arg::with_name("verbose")
                .long("verbose")
                .short("-v")
                .multiple(true)
                .help("Verbose")
        )
        .arg(
            Arg::with_name("udp")
                .long("udp")
                .help("Use of UDP instead of TCP")
        )
        .arg(
            Arg::with_name("cipher")
                .long("cipher")
                .help("Encryption algorithm requested to server.")
                .possible_values(&["rc4", "aes128", "aes256"])
                .takes_value(true)
        )
    }


    fn main(&mut self, args: ArgMatches) {
        let options;
        match AsRepRoastArgsParser::parse(&args) {
            Ok(opts) => {
                options = opts
            },
            Err(error) => {
                println!("[X] {}", error);
                return;
            }
        }

        AsRepExecutor::execute(options)
    }
}