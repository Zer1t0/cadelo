use std::sync::*;
use ascii::AsciiString;
use kerbeiros;
use crate::bruter::*;

pub struct ResultsHandler {
    preauth_discovered_users_count: usize,
    error_count: usize,
    reporter: Box<dyn Reporter>,
    stop_the_attack: Arc<RwLock<bool>>
}

impl ResultsHandler {
    
    pub fn execute(
        receiver: mpsc::Receiver<BruterResult>,
        reporter: Box<dyn Reporter>,
        stop_the_attack: Arc<RwLock<bool>>
    ) {
        let mut collector = Self::new(
            reporter, stop_the_attack
        );
        collector.handle_results(receiver);
        collector.close_execution();
    }

    fn new(
        reporter: Box<dyn Reporter>,
        stop_the_attack: Arc<RwLock<bool>>
    ) -> Self {
        return Self{
            preauth_discovered_users_count: 0,
            error_count: 0,
            reporter,
            stop_the_attack
        };
    }

    fn handle_results(&mut self, receiver: mpsc::Receiver<BruterResult>) {
        for result in receiver {
            self.handle_result(result);
        }
    }

    fn close_execution(&mut self) {
        let non_preauth_count = self.count_non_preauth();
        self.reporter.report_attack_summary(None, non_preauth_count);
    }

    fn count_non_preauth(&self) -> usize {
        return self.preauth_discovered_users_count;
    }

    fn handle_result(&mut self, result: BruterResult) {
        match result {
            BruterResult::InvalidUser(username) => {
                self.handle_invalid_username(username);
            }
            BruterResult::DisabledUser(username) => {
                self.handle_disabled_user(username);
            }
            BruterResult::NonPreauthUser(username, asrep) => {
                self.handle_non_preauth_user(username, asrep);
            }
            BruterResult::ValidUser(username) => {
                self.handle_valid_user(username);
            }
            BruterResult::Error(username, password, error) => {
                self.handle_error(username, password, error);
            }
            BruterResult::KrbError(username, password, krb_error) => {
                self.handle_krb_error(username, password, krb_error)
            }
            _ => unreachable!()
        }
    }

    fn handle_invalid_username(&mut self, username: AsciiString) {
        self.reset_error_count();
        self.reporter.report_invalid_username(&username);
    }

    fn handle_disabled_user(&mut self, username: AsciiString) {
        self.reset_error_count();
        self.reporter.report_disabled_user(&username);
    }

    fn handle_non_preauth_user(&mut self, username: AsciiString, asrep: kerbeiros::AsRep) {
        self.reset_error_count();
        self.reporter.report_non_preauth_user(&username, &asrep);
        self.preauth_discovered_users_count += 1;
    }

    fn handle_valid_user(&mut self, username: AsciiString) {
        self.reset_error_count();
        self.reporter.report_valid_username(&username);
    }

    fn handle_krb_error(&mut self, username: AsciiString, password: String, krb_error: kerbeiros::KrbError) {
        self.reporter.report_krb_error(&username, &password, &krb_error);
        self.increase_error_count();
        if self.are_there_too_many_errors() {
            let mut stop_the_attack = self.stop_the_attack.write().unwrap();
            *stop_the_attack = true;
            self.reporter.report_attack_stopped_by_errors();
        }
    }

    fn handle_error(&mut self, username: AsciiString, password: String, error: kerbeiros::Error) {
        self.reporter.report_error(&username, &password, &error);
        self.increase_error_count();
        if self.are_there_too_many_errors() {
            let mut stop_the_attack = self.stop_the_attack.write().unwrap();
            *stop_the_attack = true;
            self.reporter.report_attack_stopped_by_errors();
        }
    }

    fn reset_error_count(&mut self) {
        self.error_count = 0;
    }

    fn increase_error_count(&mut self) {
        self.error_count += 1;
    }

    fn are_there_too_many_errors(&self) -> bool {
        return self.error_count > 5;
    }

}
