use std::thread;
use std::sync::*;
use threadpool::ThreadPool;
use itertools::Itertools;
use ascii::AsciiString;
use std::net::IpAddr;
use crate::bruter::*;
use super::result_handler::ResultsHandler;
use super::user_tester::UserNonPreauthTester;
use crate::args::Cipher;

pub struct Bruter {
    realm: Arc<AsciiString>,
    kdc_ip: Arc<IpAddr>,
    usernames: Vec<AsciiString>,
    threads_count: usize,
    cipher: Option<Cipher>,
    transport_protocol: kerbeiros::TransportProtocol,
    stop_the_attack: Arc<RwLock<bool>>
}

impl Bruter {

    fn new(
        usernames: Vec<AsciiString>, threads_count: usize, realm: AsciiString, kdc_ip: IpAddr,
        transport_protocol: kerbeiros::TransportProtocol, cipher: Option<Cipher>,
    ) -> Self {
        let usernames = usernames.into_iter().unique().collect();

        return Self{
            usernames,
            threads_count,
            realm: Arc::new(realm),
            kdc_ip: Arc::new(kdc_ip),
            cipher,
            transport_protocol,
            stop_the_attack: Arc::new(RwLock::new(false))
        }
    }


    pub fn execute(
        usernames: Vec<AsciiString>, threads_count: usize, realm: AsciiString, kdc_ip: IpAddr,
        cipher: Option<Cipher>, transport_protocol: kerbeiros::TransportProtocol, reporter: Box<dyn Reporter>, 
    ) {
        let bruter = Self::new(usernames, threads_count, realm, kdc_ip, transport_protocol, cipher);
        let (result_sender, result_receiver) = mpsc::channel::<BruterResult>();
        let collector_handle = bruter.spawn_collector(reporter, result_receiver);
        let testers_pool_handle = bruter.spawn_testers(result_sender);

        testers_pool_handle.join();
        collector_handle.join().unwrap();
    }

    fn spawn_collector(&self, 
        reporter: Box<dyn Reporter>, 
        result_receiver: mpsc::Receiver<BruterResult>
    ) -> thread::JoinHandle<()> {
        let stop_the_attack = self.stop_the_attack.clone();
        let collector_handle = thread::spawn(move || {
            ResultsHandler::execute(result_receiver, reporter, stop_the_attack);
        });

        return collector_handle;
    }

    fn spawn_testers(&self, result_sender: mpsc::Sender<BruterResult>) -> ThreadPool {
        let pool = ThreadPool::new(self.threads_count);

        for username in self.usernames.iter(){
            let username = username.clone();
            let realm = self.realm.clone();
            let kdc_ip = self.kdc_ip.clone();
            let stop_the_attack = self.stop_the_attack.clone();
            let sender = result_sender.clone();
            let transport_protocol = self.transport_protocol;
            let cipher = self.cipher;
            pool.execute(move || {
                UserNonPreauthTester::execute(
                    username, realm, kdc_ip, cipher, transport_protocol, stop_the_attack, sender);
            });
        }
        drop(result_sender);

        return pool;
    }
}


